async function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

const corejs = require("./plugins/cmdCore.js");

const mf = require('mineflayer');

const options = [{
    host: 'chipmunk.land',
    port: 25565,
    username: '§§ChatBridge1§§',
    version: '1.19.1',
    showIpAs: "server1"
  },{
    host: 'kaboom.pw',
    port: 25565,
    username: '§§ChatBridge2§§',
    version: '1.19.1',
    showIpAs: "server2"
  }]

const bot = mf.createBot(options[0]);

const bot2 = mf.createBot(options[1]);

bot.on('login', async () => {
    corejs.injectTo(bot)
    bot.creative.startFlying()
    await sleep(500)
    bot.chat("/v on")
    await sleep(500)
    bot.chat("/cspy on")
    await sleep(500)
    bot.chat("/god on")
})

bot2.on('login', async () => {
    corejs.injectTo(bot2)
    bot.creative.startFlying()
    await sleep(500)
    bot2.chat("/v on")
    await sleep(500)
    bot2.chat("/cspy on")
    await sleep(500)
    bot2.chat("/god on")
})

setTimeout(() => {
bot2.on('chat', (username, message, translate, jsonMsg, matches) => {
    bot.cmdCore.run(`tellraw @a[tag=!noChatBridge] ${JSON.stringify([{"text":"§ § [§ IP§ ]","hoverEvent":{"action":"show_text","value":[{"text":"This message is from a different server. The server's IP is: " + options[1].host + ":" + options[1].port + ". Click here to copy the server's IP to your clipboard."}]},"clickEvent":{"action":"copy_to_clipboard","value":"" + options[1].host + ":" + options[1].port + ""}},{"text":" ","hoverEvent":{"action":"show_text","value":[{"text":""}]},"clickEvent":{"action":"run_command","value":""}},{"text":"§ § [§ NM§ ]","hoverEvent":{"action":"show_text","value":[{"text":"Click here to mute all messages from this server."}]},"clickEvent":{"action":"run_command","value":"/tag @s add noChatBridge"}},{"text":" ","hoverEvent":{"action":"show_text","value":[{"text":""}]},"clickEvent":{"action":"run_command","value":""}},{"text":"" + `${jsonMsg}` + "","hoverEvent":{"action":"show_text","value":[{"text":""}]},"clickEvent":{"action":"run_command","value":""}}])}`)
    console.log('someone chatted')
})

bot.on('chat', (username, message, translate, jsonMsg, matches) => {
    bot2.cmdCore.run(`tellraw @a[tag=!noChatBridge] ${JSON.stringify([{"text":"§ § [§ IP§ ]","hoverEvent":{"action":"show_text","value":[{"text":"This message is from a different server. The server's IP is: " + options[0].host + ":" + options[0].port + ". Click here to copy the server's IP to your clipboard."}]},"clickEvent":{"action":"copy_to_clipboard","value":"" + options[0].host + ":" + options[0].port + ""}},{"text":" ","hoverEvent":{"action":"show_text","value":[{"text":""}]},"clickEvent":{"action":"run_command","value":""}},{"text":"§ § [§ NM§ ]","hoverEvent":{"action":"show_text","value":[{"text":"Click here to mute all messages from this server."}]},"clickEvent":{"action":"run_command","value":"/tag @s add noChatBridge"}},{"text":" ","hoverEvent":{"action":"show_text","value":[{"text":""}]},"clickEvent":{"action":"run_command","value":""}},{"text":"" + `${jsonMsg}` + "","hoverEvent":{"action":"show_text","value":[{"text":""}]},"clickEvent":{"action":"run_command","value":""}}])}`)
    console.log('someone chatted')
})

}, 1000);

bot.on('login', () => {
    console.log("connected")
})